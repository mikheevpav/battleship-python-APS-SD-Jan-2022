import random

import colorama
from colorama import Fore, Back, Style

from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController

myFleet = []
enemyFleet = []

def cprint(color, msg):
    print(color + msg + Style.RESET_ALL)

def main():
    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    initialize_game()

    start_game()

def start_game():
    global myFleet, enemyFleet

   #  print(Fore.GREEN + r'''
   #                __
   #               /  \
   #         .-.  |    |
   # *    _.-'  \  \__/
   #  \.-'       \
   # /          _/
   # |      _  /
   # |     /_\
   #  \    \_/
   #   """"""""''')
    print(r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')

    while True:
        print()
        cprint(color=Fore.YELLOW, msg="=================================")
        cprint(color=Fore.GREEN, msg="Player, it's your turn:")
        cprint(color=Fore.GREEN, msg="Possible actions: hit")
        position = parse_position(input(Fore.WHITE + "Enter coordinates for your shot :" + Style.RESET_ALL))
        is_hit = GameController.check_is_hit(enemyFleet, position)
        killed, alive = GameController.ship_states(ships=enemyFleet)
        if GameController.check_win(alive):
            cprint(color=Fore.CYAN, msg="YOU WIN! GAME OVER")
            return
        print("Killed:", killed)
        print("Alive:", alive)

        if is_hit:
            cprint(Fore.RED, r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')
            cprint(Fore.GREEN, "Yeah ! Nice hit !")
        else:
            cprint(Fore.BLUE, r'''                                ,d                          
                                88                          
8b      db      d8 ,adPPYYba, MM88MMM ,adPPYba, 8b,dPPYba,  
`8b    d88b    d8' ""     `Y8   88   a8P_____88 88P'   "Y8  
 `8b  d8'`8b  d8'  ,adPPPPP88   88   8PP""""""" 88          
  `8bd8'  `8bd8'   88,    ,88   88,  "8b,   ,aa 88          
    YP      YP     `"8bbdP"Y8   "Y888 `"Ybbd8"' 88  ''')
            cprint(Fore.GREEN, "Miss")

        cprint(color=Fore.WHITE, msg="=================================")
        position = get_random_position()
        is_hit = GameController.check_is_hit(myFleet, position)
        killed, alive = GameController.ship_states(ships=myFleet)
        if GameController.check_win(alive):
            cprint(color=Fore.MAGENTA, msg="YOU LOOSE! GAME OVER")
            return
        print()
        cprint(Fore.GREEN, f"Computer shoot in {position.column.name}{position.row} and {'hit your ship!' if is_hit else 'miss'}")
        if is_hit:
            cprint(Fore.RED, '''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')

def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)

def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position

def initialize_game():
    initialize_myFleet()

    initialize_enemyFleet()

def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()

    print("Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        print()
        print(f"Please enter the positions for the {ship.name} (size: {ship.size})")

        for i in range(ship.size):
            position_input = input(f"Enter position {i} of {ship.size} (i.e A3):")

            ship.add_position(position_input)

def initialize_enemyFleet():
    global enemyFleet

    enemyFleet = GameController.initialize_ships()

    enemyFleet[0].positions.append(Position(Letter.B, 4))
    enemyFleet[0].positions.append(Position(Letter.B, 5))
    enemyFleet[0].positions.append(Position(Letter.B, 6))
    enemyFleet[0].positions.append(Position(Letter.B, 7))
    enemyFleet[0].positions.append(Position(Letter.B, 8))

    enemyFleet[1].positions.append(Position(Letter.E, 6))
    enemyFleet[1].positions.append(Position(Letter.E, 7))
    enemyFleet[1].positions.append(Position(Letter.E, 8))
    enemyFleet[1].positions.append(Position(Letter.E, 9))

    enemyFleet[2].positions.append(Position(Letter.A, 3))
    enemyFleet[2].positions.append(Position(Letter.B, 3))
    enemyFleet[2].positions.append(Position(Letter.C, 3))

    enemyFleet[3].positions.append(Position(Letter.F, 8))
    enemyFleet[3].positions.append(Position(Letter.G, 8))
    enemyFleet[3].positions.append(Position(Letter.H, 8))

    enemyFleet[4].positions.append(Position(Letter.C, 5))
    enemyFleet[4].positions.append(Position(Letter.C, 6))

if __name__ == '__main__':
    main()
